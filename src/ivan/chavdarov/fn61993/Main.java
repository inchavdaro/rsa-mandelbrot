package ivan.chavdarov.fn61993;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class Main {

    private static Options generateOptions() {
        Option size = new Option("s", "size", true, "Size of the image.");
        Option rect = new Option("r", "rect", true, "coordinate plane");
        Option tasks = new Option("t", "tasks", true, "Number of the threads.");
        Option output = new Option("o", "output", true, "Name of the image.");
        Option quiet = new Option("q", "quiet", false, "Quiet mode.");
        return new Options()
                        .addOption(size)
                        .addOption(rect)
                        .addOption(tasks)
                        .addOption(output)
                        .addOption(quiet);
    }

    private static CommandLine generateCommandLine(Options options, String[] args)
    {
        CommandLineParser parser = new DefaultParser();
        try {
            return parser.parse(options, args);
        } catch(ParseException ex) {
            throw new IllegalArgumentException("Parsing error: " + ex.getMessage());
        }
    }

    public static void main(String[] args) {
        Options options = generateOptions();
        CommandLine commandLine = generateCommandLine(options,args);

        new FractalImage(commandLine).generate();
    }
}
