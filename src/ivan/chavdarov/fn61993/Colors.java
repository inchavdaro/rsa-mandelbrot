package ivan.chavdarov.fn61993;


import com.sap.jvm.profiling.ui.graph.renderer.Color;

import java.util.ArrayList;
import java.util.List;

public class Colors {

    private static List<Color> colors = new ArrayList<Color>();
    static {
        colors.add(new Color(0,0,0));
        for (int r=0; r<255; r++) colors.add(new Color(255-r, 255, 0));
        for (int g=100; g>0; g--) colors.add(new Color(      255, 255-g,         0));
        for (int b=0; b<255; b++) colors.add(new Color(      255,         0, 255-b));
        for (int r=100; r>0; r--) colors.add(new Color(255-r,         0,       255));
        for (int g=0; g<255; g++) colors.add(new Color(        0, 255-g,       255));
    }

    public static int getColorRGB(int number){
        if(colors.size() > number){
            return colors.get(number).rgb;
        }else{
            return 0x00000;
        }
    }
}

