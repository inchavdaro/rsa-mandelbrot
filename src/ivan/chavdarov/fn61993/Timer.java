package ivan.chavdarov.fn61993;

public class Timer {
    private long time;

    long stop() {
        long currentTimeMillis = System.nanoTime();
        return (currentTimeMillis - time);
    }

    private long getCurrentTime()
    {
        return System.nanoTime();
    }

    public void start()
    {
        time = getCurrentTime();
    }
}
