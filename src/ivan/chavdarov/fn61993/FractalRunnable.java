package ivan.chavdarov.fn61993;

import org.apache.commons.math3.complex.Complex;

import java.awt.image.BufferedImage;

public class FractalRunnable implements Runnable {

    private BufferedImage image;

    private int x;

    private int y;

    private double minX;

    private double maxX;

    private double minY;

    private double maxY;

    private int depth;

    private int taskNumber;

    private boolean quiet;

    public FractalRunnable(BufferedImage image, int x, int y, double minX, double maxX, double minY, double maxY,
                           int taskNumber, boolean quiet) {
        this.image = image;
        this.x = x;
        this.y = y;
        this.minX = minX;
        this.maxX = maxX;
        this.minY = minY;
        this.maxY = maxY;
        this.taskNumber = taskNumber;
        this.quiet = quiet;
        this.depth = Math.max(image.getWidth(),image.getHeight());
    }

    @Override public void run() {

        Timer timer = new Timer();
        if (!quiet) {
            timer.start();
        }

        double w = image.getWidth();
        double h = image.getHeight();

        if(taskNumber > 160000){
            int a = 10;
        }
        double real = (x - w/maxX)*((maxX-minX)/w);
        //double real = (x - w/2.0)*(4.0/w);
        double imaginary = (y - h/maxY)*((maxY-minY)/h);
        //double imaginary = (y - h/2.0)*(4.0/h);

        int stepCount = check(new Complex(real,imaginary));

        if(stepCount < depth){
            image.setRGB(x,y,Colors.getColorRGB(stepCount));
        }else {
            image.setRGB(x,y,Colors.getColorRGB(0));
        }

        if(!quiet){
            System.out.println("Calculation:" + taskNumber + " took " + timer.stop() + " nanoseconds.");
        }

    }

    private int check(Complex number){
        int stepCount = 0;

        double escapeRadius = 2.0;

        Complex z = new Complex(0.0,0.0);

        while (z.abs() <= escapeRadius && stepCount < depth){
            z = z.multiply(z).multiply(z.multiply(z).exp()).add(number);

            stepCount++;
        }

        return stepCount;
    }
}
