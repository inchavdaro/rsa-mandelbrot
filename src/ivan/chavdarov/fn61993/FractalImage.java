package ivan.chavdarov.fn61993;

import org.apache.commons.cli.CommandLine;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class FractalImage {

    private int width = 640;

    private int height = 480;

    private double minX = -2.0;

    private double maxX = 2.0;

    private double minY = -2.0;

    private double maxY = 2.0;

    private int threads = 1;

    private String filename = "mandelbrot.png";

    private boolean quiet = false;

    public FractalImage(CommandLine cmd) {
        if (cmd.hasOption("s")) {
            String arg = cmd.getOptionValue("s");
            String[] args = arg.split("x");
            width = Integer.parseInt(args[0]);
            height = Integer.parseInt(args[1]);

            if (width < 0 || height < 0) {
                throw new IllegalArgumentException("width and height must be positive integers");
            }
        }

        if (cmd.hasOption("r")) {
            String arg = cmd.getOptionValue("r");
            String[] args = arg.split(":");
            minX = Double.parseDouble(args[0]);
            maxX = Double.parseDouble(args[1]);
            minY = Double.parseDouble(args[2]);
            maxY = Double.parseDouble(args[3]);
        }

        if (cmd.hasOption("t")) {
            threads = Integer.parseInt(cmd.getOptionValue("t"));

            if (threads < 1) {
                throw new IllegalArgumentException("tasks must be bigger than 0");
            }
        }

        if (cmd.hasOption("o")) {
            filename = cmd.getOptionValue("o");
        }

        if (cmd.hasOption("q")) {
            quiet = true;
        }
    }

    public void generate() {

        Timer timer = new Timer();
        timer.start();

        System.out.println("Execution started");

        BufferedImage image = new BufferedImage(this.width, this.height, BufferedImage.TYPE_INT_RGB);

        ExecutorService executor = Executors.newFixedThreadPool(threads);

        AtomicInteger taskNumber = new AtomicInteger(0);

        for (int i = 0; i < width; i++) {

            for (int j = 0; j < height; j++) {
                executor.execute(new FractalRunnable(image, i, j, minX, maxX, minY, maxY, taskNumber.incrementAndGet(),
                                                    quiet));
            }
        }

        executor.shutdown();
        while (!executor.isTerminated()) {
        }


        try {
            ImageIO.write(image, "png", new File(filename));
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.printf("Execution took: " + (float) timer.stop() / 1000000000+ " seconds with: " + threads + " threads");
    }
}
